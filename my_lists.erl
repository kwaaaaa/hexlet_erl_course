-module(my_lists).
-export([any/2, all/2, len/1, reverse/1, member/2, filter/2,
 dropwhile/2, takewhile/2, splitwith/2, zipwith/3]).


any(_, []) ->
	false;

any(Pred, List) ->
	[Head | Tail] = List,
	case Pred(Head) of
		true -> true;
		false -> any(Pred, Tail)
	end.


all(_, []) ->
	true;

all(Pred, List) ->
	[Head | Tail] = List,
	case Pred(Head) of
		false -> false;
		true -> all(Pred, Tail)
	end.


len(List) -> len(List, 0).

len([], Acc) -> Acc;

len([_ | T], Acc) -> len(T, Acc + 1).
	

reverse(List) -> reverse(List, []).

reverse([], Acc) -> Acc;

reverse([H | T], Acc) -> reverse(T, [H | Acc]).


member(_, []) -> false;

member(Elem, List) ->
	[Head | Tail] = List,
	case Elem =:= Head of
		true -> true;
		false -> member(Elem, Tail)
	end.


filter(_, []) -> [];

filter(Pred, List) -> filter(Pred, List, []).

filter(_, [], Acc) -> reverse(Acc);

filter(Pred, [H|T], Acc) ->
	case Pred(H) of
		true -> filter(Pred, T, [H|Acc]);
		false -> filter(Pred, T, Acc)
	end.

dropwhile(_, []) -> [];
dropwhile(Pred, List) ->
	[H|T] = List,
	case Pred(H) of
		true -> dropwhile(Pred, T);
		false -> List
	end.

takewhile(_, []) -> [];
takewhile(Pred, List) -> takewhile(Pred, List, []).

takewhile(_, [], Acc) -> reverse(Acc);
takewhile(Pred, [H|T], Acc) ->
	case Pred(H) of
		true -> takewhile(Pred, T, [H|Acc]);
		false -> reverse(Acc)
	end.

splitwith(Pred, List) ->
    {takewhile(Pred, List), dropwhile(Pred, List)}.

zipwith(Pred, List1, List2) -> zipwith(Pred, List1, List2, []).

zipwith(_, [], [], List3) -> reverse(List3);
zipwith(_, [], List2, List3) -> reverse(List3) ++ List2;
zipwith(_, List1, [], List3) -> reverse(List3) ++ List1;
zipwith(Pred, [H1|T1], [H2|T2], List3) ->
	zipwith(Pred, T1, T2, [Pred(H1, H2)|List3]).