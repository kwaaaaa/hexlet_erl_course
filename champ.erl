-module(champ).
-export([sample_champ/0, get_stat/1, filter_sick_players/1, make_pairs/2]).

sample_champ() ->
	[
    {team, "Crazy Bulls",
        [{player, "Big Bull", 22, 545, 100},
         {player, "Small Bull", 18, 324, 95}]},
    {team, "Cool Horses",
        [{player, "Lazy Horse", 21, 423, 80},
         {player, "Sleepy Horse", 23, 101, 55}]}
	].

get_stat(Champ) ->
	PlayerStatFun = fun({player, _, Age, Rating, _}, {AccNumPlayers, AccAge, AccRating}) ->
		{AccNumPlayers + 1, AccAge + Age, AccRating + Rating}
	end,
	TeamStatFun = fun(Team, Acc) ->
		{team, _, Players} = Team,
		{ChampNumPlayers, ChampTotalAge, ChampTotalRating} = Acc,
		{TeamNumPlayers, TeamAge, TeamRating} = lists:foldl(PlayerStatFun, {0, 0, 0}, Players),
		{ChampNumPlayers + TeamNumPlayers, ChampTotalAge + TeamAge, ChampTotalRating + TeamRating}
	end,	
	{ChampNumPlayers, ChampTotalAge, ChampTotalRating} = lists:foldl(TeamStatFun, {0, 0, 0}, Champ),
	{length(Champ), ChampNumPlayers, ChampTotalAge / ChampNumPlayers, ChampTotalRating / ChampNumPlayers}.


filter_sick_players(Champ) ->
  FilterHealthuyPlayersFun = fun(Player, HealthyPlayers) ->
  	{player, _, _, _, Health} = Player,
  	if 
			Health >= 50 ->
				lists:append([HealthyPlayers, [Player]]);
			true ->
				HealthyPlayers
    end
	end,
	FilterHealthuyTeamsFun = fun(Team, HealthyTeams) ->
		{team, Name, Players} = Team,
		HealthyPlayers = lists:foldl(FilterHealthuyPlayersFun, [], Players),
		HealthyCount = length(HealthyPlayers),
		if 
			HealthyCount >= 5 ->
			lists:append([HealthyTeams, [{team, Name, HealthyPlayers}]]);
			true ->
				HealthyTeams
    end
	end,	
	lists:foldl(FilterHealthuyTeamsFun, [], Champ).

% [T1, T2, T3, T4, _] = champ:sample_champ().

make_pairs(Team1, Team2) ->
	{team, _, Players1} = Team1,
	{team, _, Players2} = Team2,
	[{Name1,Name2} || {player, Name1, _, Rating1, _} <- Players1, {player, Name2, _, Rating2, _} <- Players2, Rating1 + Rating2 > 600].



